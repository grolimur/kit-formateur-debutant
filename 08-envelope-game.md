# Le jeu de l'enveloppe

### Description

"Le jeu de l'enveloppe est particulièrement adapté à la recherche de solutions à plusieurs problèmes donnés." (Hourst, 2015, p. 79)

Cet exercice comporte une étape un peu délicate: le 1er groupe qui traite un problème ne doit pas chercher des solutions; ce groupe doit définir les critères qui vont servir à évaluer les solutions des autres groupes.

### Taille du groupe

Cette méthode est adaptée à des groupes de la taille d'une classe (15 à 30 personnes).

### Temps nécessaire

Il faut compter 20-25 minutes par groupe. Ainsi, si vous formez 4 groupes, l'exercice durera entre 1h20 et 1h45 (mise en commun et correction comprises). Le temps de préparation est de l'ordre de 1-2h.

### Exemple

Vous voulez faire comprendre aux étudiants comment fonctionne la publication scientifique.   
Vous décidez de les mettre dans les souliers d'un éditeur scientifique qui crée une nouvelle revue scientifique.   
Vous répartissez les étudiants en 4 groupes et vous leur donnez les 4 points suivants à régler:

1. lister les directives à fournir aux auteurs
2. choisir la modèle de validation des articles soumis (quel *peer review* ?)
3. définir le personnel nécessaire pour faire tourner le journal
4. décider du modèle économique (journal traditionnel ou *Open Access*, prix pratiqués, etc.)

Chacun de ses problèmes n'a pas qu'une seule solution correcte.
